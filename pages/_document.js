import Document, { Head, Main, NextScript } from "next/document";
import Page from "../layouts/Page";

export default class MyDocument extends Document {
  state = { isOpen: false };
  render() {
    return (
      <html>
        <Head>
          <link rel="stylesheet" href="/bootstrap.min.css" />
        </Head>
        <body>
          <Page>
            <Main />
          </Page>
          <NextScript />
        </body>
      </html>
    );
  }
}
