import React from "react";
import { Container, Spinner, Table } from "reactstrap";
import Head from "next/head";
import useSwr from "swr";

const fetcher = url => fetch(url).then(res => res.json())

export default () => {
  const { data, error } = useSwr('/api/stonks', fetcher)
  if (error) return <h3>Failed to load stonks</h3>

  return (
    <>
      <Head>
        <title>Stonks.</title>
      </Head>

      <section className="py-5" style={{ paddingBottom: "50px", background: "#f5f3f6" }}>
        <Container className="py-5 content">
          <h2 className="h1-responsive font-weight-bold text-center my-3">
            Stocks
          </h2>
          <p className="lead grey-text text-center w-responsive mx-auto mb-5">
            Our app is unique among others as it combines the realtime data and the best practices.
          </p>

          {!data ? (
            <div className="d-flex justify-content-center py-5"><Spinner /></div>
          ) : (
            <Table>
              <thead>
              <tr>
                <th>#</th>
                <th>Pair</th>
                <th>Rate</th>
              </tr>
              </thead>
              <tbody>
                {Object.entries(data.rates).map(([pair, rate], id) => (
                  <tr key={pair}>
                    <th scope="row">{id}</th>
                    <td>{data.base} — {pair}</td>
                    <td>{rate}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          )}
        </Container>
      </section>
    </>
  );
}
