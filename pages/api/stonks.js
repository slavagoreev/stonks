import fetch from 'node-fetch';

export default async (req, res) => {
  res.statusCode = 200
  res.setHeader('Content-Type', 'application/json');
  const response = await fetch('https://api.exchangeratesapi.io/latest');
  const data = await response.json();
  res.end(JSON.stringify(data))
}
