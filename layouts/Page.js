import {
  Col,
  Collapse,
  Container,
  Jumbotron,
  Nav,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  NavItem,
  NavLink, Row
} from "reactstrap";
import React, { useState } from 'react';
import Head from "next/head";

const tasks = [
  { link: '/', label: 'Stonks' },
  { link: 'https://t.me/slavagoreev', label: 'About us' }
];

const Page = ({ children }) => {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
  return (
    <>
      <Navbar light expand="md" className="position-fixed w-100">
        <Container>
          <NavbarBrand href="/">Stonks.</NavbarBrand>
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar className="ml-3">
            <Nav navbar>
              {tasks.map(task => (
                <NavItem key={task.link}>
                  <NavLink href={task.link}>{task.label}</NavLink>
                </NavItem>
              ))}
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
      <div style={{ background: "rgb(230, 229, 230)" }}>
        <Jumbotron fluid className="mb-0 bg-transparent">
          <Container className="py-5">
            <Row>
              <Col md={5}>
                <img src="/logo.svg" alt="" className="w-100" />
              </Col>
              <Col md={{ offset: 1, span: 5 }} className="my-auto">
                <h1 className="display-4 d-flex">
                  <strong>Stonks.</strong>
                </h1>
                <p className="lead">
                  The ultimate app that helps to get<br /> the current stonks in a second.
                </p>
              </Col>
            </Row>
          </Container>
        </Jumbotron>
      </div>
      {children}
    </>
  );
};

export default Page;
